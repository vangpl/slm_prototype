# SLM Prototype (M8 edition)
Denne prototypen bruker Sass og Jade til å generere CSS og HTML via Gulp. Følg disse stegene for å få rammeverket opp og kjøre (hopp over stegene hvis du allerede har det installert globalt på maskinen):

1. Installér [NodeJS](https://nodejs.org/).
2. Installér [Bower](http://bower.io/) ved å åpne Terminal og skrive `sudo npm install -g bower` + enter etterfulgt av passordet ditt.
3. Installér [Gulp](http://gulpjs.com/) ved å skrive `sudo npm install -g gulp` + enter
4. Navigér til prosjektmappen (rot-mappen i dette repoet) med terminal (Mac), du kan enten navigere dit med `cd mappe/undermappe/` (skriv `ls` for å se tilgjengelige undermapper), eller du kan åpne Terminal og folderen i Finder for så så skrive `cd` + mellomrom og så dra folderikonet i Finder over i Terminal.
5. Skriv `bower install` + enter for å installere bower-dependencies
6. Skriv `npm install` + enter for å installere node-dependencies
7. Kompilér kildefiler ved å skrive `gulp` + enter. Dette vil også starte «watch»-tasken som lytter etter endringer i jade-, sass-, img- og js-mappene. Hver gang en endring skjer i en av disse mappene vil oppdaterte kildefiler genereres. Ved senere anledninger når kildefilene er generert lokalt kan du kjøre `gulp watch` for å bare lytte etter filendringer.
8. Start MAMP eller tilsvarende lokal server og se prototypen i aksjon.
