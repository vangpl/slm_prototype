var gulp = require('gulp');

var jade = require('gulp-jade');
var data = require('gulp-data');
var plumber = require('gulp-plumber');
var util = require('gulp-util');
var fs = require('fs');

gulp.task('jade', function () {
  return gulp.src(['jade/**/*.jade', '!**/includes{,/**}'])
    .pipe(plumber({
      errorHandler: function(error) {
        util.log(
          util.colors.cyan('Plumber') + util.colors.red(' found unhandled error:\n'),
          error.toString()
        );
        this.emit('end');
      }
    }))
    .pipe(data(function (file) {
      var relativePath = file.history[0].replace(file.base, ''); //e.g. about/index.jade
      var depth = (relativePath.match(/\//g) || []).length; //e.g. 1
      var relativeRoot = new Array(depth + 1).join( '../' ); //e.g. ../
      var menu = JSON.parse(fs.readFileSync('./jade/data/menu.json'));
      return {
        relativeRoot: relativeRoot,
        menu: menu
      };
    }))
    .pipe(jade({
      pretty: true
    }))
    .pipe(plumber.stop())
    .pipe(gulp.dest('public_html'))
});
