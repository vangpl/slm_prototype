var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');

var plumber = require('gulp-plumber');
var util = require('gulp-util');


gulp.task('js', function() {

  // Main JS-scrips, loaded in body footer
  gulp.src([
      'bower_components/Stickyfill/dist/stickyfill.min.js', // Position sticky polyfill
      'bower_components/better-dom/dist/better-dom.js',  // Date input polyfill
      'bower_components/better-i18n-plugin/dist/better-i18n-plugin.js', // Date input polyfill
      'bower_components/better-dateinput-polyfill/dist/better-dateinput-polyfill.js', // Date input polyfill
      'js/main.js',
      'js/i18n/main.no.js',  // Date input polyfill
      'js/newsletter-popup.js'
    ])
    .pipe(plumber({
      errorHandler: function(error) {
        util.log(
          util.colors.cyan('Plumber') + util.colors.red(' found unhandled error:\n'),
          error.toString()
        );
        this.emit('end');
      }
    }))
    //.pipe(sourcemaps.init())
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(plumber.stop())
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest('public_html/js/'));

  // For Old IE, loaded in head
  gulp.src([
      'bower_components/html5shiv/dist/html5shiv-printshiv.min.js'
    ])
    .pipe(concat('oldIE.js'))
    .pipe(gulp.dest('public_html/js/'));

  gulp.src('bower_components/jquery/dist/jquery.min.js')
    .pipe(gulp.dest('public_html/js/'));

  gulp.src([
      'bower_components/magnific-popup/dist/jquery.magnific-popup.min.js'
    ])
    .pipe(gulp.dest('public_html/js/'));
});
