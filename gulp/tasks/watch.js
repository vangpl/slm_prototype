var gulp = require('gulp');

gulp.task('watch', ['setWatch'], function() {

  gulp.watch('scss/**/*.scss', ['sass']);

  gulp.watch('img/sprite/svg/**/*.svg', ['sprite']);

  gulp.watch('jade/**/*.jade', ['jade']);
  gulp.watch('jade/**/*.json', ['jade']);

  gulp.watch(['js/**/*.js', 'bower_components/**/*.js'], ['js']);

});
