(function($) {
  'use strict';

  $(document).ready(function() {
    var cookieName = 'slmNewsletterModalShown' + encodeURI(location.href.split( '/' )[2]);

    if( !readCookie(cookieName) && $('#js-newsletter-subscribe').length ) {
      window.setTimeout(function(){
        $.magnificPopup.open({
          items: {
            src: '#js-newsletter-subscribe',
            type: 'inline'
          },
          callbacks: {
            close: function() {
              createCookie(cookieName, true, 10000);
            }
          }
        });

        $('.js-nl-modal-close').click(function(e) {
          e.preventDefault();
          $.magnificPopup.close();
        });

        $('.js-nl-modal-form').on('submit', function(e){
          var form = $(this),
              input = form.find('.js-nl-modal-input'),
              inputVal = input.val(),
              regEx = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
          e.preventDefault();

          form.find('.warning').remove();

          if(inputVal.length && regEx.test(inputVal)) {
            form.unbind('submit').submit();
            $.magnificPopup.close();
          } else {
            input.after('<div class="warning">Du må fylle inn en gyldig e-postadresse</div>');
            input.focus();
          }

        });
      }, 15000);
    }
  });

  function createCookie(name,value,days) {
    if (days) {
      var date = new Date();
      date.setTime(date.getTime()+(days*24*60*60*1000));
      var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
  }

  function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
  }

})(jQuery);
